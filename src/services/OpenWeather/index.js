import axios from "axios";
const API_KEY = "b6479677dd0039d4731af3bc21201f37";

const axiosInstance = axios.create({
  baseURL: "https://api.openweathermap.org",
  timeout: 16000
});

export function loadWeather(city) {
  return axiosInstance
    .get(`/data/2.5/weather?q=${city}&appid=${API_KEY}`)
    .then(response => response.data);
}
