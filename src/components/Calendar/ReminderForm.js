import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  getTodayDateAsString,
  isValidStringAsDateTime
} from "../../utils/Date";
import { BlockPicker } from "react-color";

class ReminderForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fields: ReminderForm.defaultReminderFields(props)
    };
  }

  static getDerivedStateFromProps = (props, state) => {
    let { editReminder } = props;
    editReminder = editReminder || {};

    if (editReminder.id !== state.fields.id) {
      if (editReminder.id) {
        return {
          fields: {
            ...state.fields,
            id: editReminder.id,
            date: editReminder.date,
            time: editReminder.time,
            city: editReminder.city,
            text: editReminder.text,
            color: editReminder.color
          }
        };
      }
      return {
        fields: {
          ...state.fields,
          ...ReminderForm.defaultReminderFields(props)
        }
      };
    }

    return { fields: { ...state.fields } };
  };

  static defaultReminderFields = props => {
    let { editReminder } = props;
    editReminder = editReminder || {};
    return {
      id: editReminder.id || undefined,
      createdAt: editReminder.createdAt || undefined,
      modifiedDate: editReminder.modifiedDate || undefined,
      date: editReminder.date || getTodayDateAsString(),
      time: editReminder.time || "00:30",
      city: editReminder.city || "London",
      text: editReminder.text || "",
      color: editReminder.color || "#d9e3f0"
    };
  };

  handleUpdateReminder = color => {
    this.setState(state => ({
      fields: { ...state.fields, color: color.hex }
    }));
  };

  handleTextReminder = event => {
    const { value = "", name } = event.target;
    this.setState(state => ({
      fields: { ...state.fields, [name]: value }
    }));
  };

  onFormSubmit = event => {
    event.preventDefault();
    const { fields } = this.state;
    const { onClose, onFormSubmit, editReminder } = this.props;

    const validDate =
      fields.date.split("-").length === 3 &&
      isValidStringAsDateTime(fields.date, fields.time);
    const validTime = fields.time.length === 5;

    if (
      validDate &&
      validTime &&
      fields.city.length > 0 &&
      fields.text.length <= 30 &&
      fields.text.length > 0 &&
      !!fields.color
    ) {
      this.clearForm();
      const modifiedDate = new Date();

      onFormSubmit(
        {
          ...fields,
          date: validDate.date,
          time: validDate.formattedTime,
          modifiedDate
        },
        editReminder
      );

      onClose();
    } else {
      let errors = [];
      if (!validDate) {
        errors.push("Date is not valid");
      }

      if (!validTime) {
        errors.push("Time is not valid");
      }

      if (fields.city.length < 1) {
        errors.push("City is not valid");
      }

      if (fields.text.length > 30 || fields.text.length < 1) {
        errors.push("Text is not valid. Max 30 characters.");
      }

      if (!fields.color) {
        errors.push("Color is not valid");
      }

      this.setState({
        errors: errors
      });
    }
  };

  clearForm = () => {
    this.setState({
      errors: [],
      fields: ReminderForm.defaultReminderFields(this.props)
    });
  };

  onMaskClick = () => {
    const { onClose } = this.props;
    this.clearForm();
    onClose();
  };

  render() {
    const { fields, errors } = this.state;
    const {
      showReminderModal,
      editReminder,
      removeReminder,
      onClose
    } = this.props;

    // dont render modal
    if (!showReminderModal) {
      return null;
    }

    return (
      <div className="reminder-modal">
        <div className="reminder-modal__mask" onClick={this.onMaskClick}></div>
        <div className="reminder-modal__content">
          <form onSubmit={this.onFormSubmit}>
            <h2>{editReminder ? "Update" : "Add"} Reminder</h2>

            {errors && errors.length > 0 && (
              <div className="reminder-modal__errors">
                {errors.map(error => {
                  return <div className="reminder-modal__error">{error}</div>;
                })}
              </div>
            )}

            <h3>Date</h3>
            <input
              value={fields.date}
              type="text"
              name="date"
              placeholder="yyyy-mm-dd"
              onChange={this.handleTextReminder}
            />
            <h3>Hour</h3>
            <input
              value={fields.time}
              type="text"
              name="time"
              placeholder="hh:mm"
              onChange={this.handleTextReminder}
            />
            <h3>City</h3>
            <input
              value={fields.city}
              type="text"
              name="city"
              placeholder="London"
              onChange={this.handleTextReminder}
            />
            <h3>Reminder</h3>
            <input
              type="text"
              maxLength={30}
              name="text"
              value={fields.text}
              placeholder="Type your reminder here"
              onChange={this.handleTextReminder}
            />

            <h3>Color:</h3>
            <BlockPicker
              onChange={this.handleUpdateReminder}
              triangle="hide"
              color={fields.color}
            />

            <button className="reminder-modal__save">Save</button>
            {editReminder && (
              <button
                className="reminder-modal__remove"
                onClick={event => {
                  event.preventDefault();
                  removeReminder(editReminder);
                  onClose();
                }}
              >
                Remove
              </button>
            )}
          </form>
        </div>
      </div>
    );
  }
}

ReminderForm.defaultProps = {
  onFormSubmit: () => {},
  onClose: () => {},
  removeReminder: () => {}
};

ReminderForm.propTypes = {
  onFormSubmit: PropTypes.func,
  onClose: PropTypes.func,
  removeReminder: PropTypes.func
};

export default ReminderForm;
