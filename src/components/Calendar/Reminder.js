import React from "react";

const CalendarReminder = props => {
  const { reminder, onClick } = props;
  const { weatherData: { weather } = {} } = reminder;

  let weatherContent;
  if (weather && weather.length > 0) {
    weatherContent = weather[0];
  }

  return (
    <div
      className="calendar__reminder"
      style={{ background: reminder.color }}
      onClick={() => {
        onClick(reminder);
      }}
    >
      <span className="calendar__reminder__time">{reminder.time}</span>
      <span className="calendar__reminder__description">{reminder.text}</span>
      {weatherContent && (
        <img
          className="calendar__reminder__icon"
          src={`http://openweathermap.org/img/wn/${weatherContent.icon}@2x.png`}
          alt={`Weather on ${reminder.city}: ${weatherContent.main}`}
          title={`Weather on ${reminder.city}: ${weatherContent.main}`}
        />
      )}
    </div>
  );
};

CalendarReminder.propTypes = {};

CalendarReminder.defaultProps = {
  reminder: {},
  onClick: () => {}
};

export default CalendarReminder;
