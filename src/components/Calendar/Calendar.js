import React, { Component, Fragment } from "react";
import CalendarDay from "./Day";
import MonthSelector from "../MonthSelector";
import { getMonthYearDate, buildCalendarMonth } from "../../utils/Date";
import ReminderForm from "./ReminderForm";

import "./style.scss";
import { loadWeather } from "../../services/OpenWeather";

class Calendar extends Component {
  constructor(props) {
    super(props);

    const date = getMonthYearDate();

    this.state = {
      showReminderModal: false,
      editReminder: null,
      reminders: {},
      currentDate: {
        month: date.getMonth(),
        year: date.getFullYear()
      }
    };
  }

  updateMonth = dateObj => {
    this.setState(
      {
        currentDate: {
          month: dateObj.month,
          year: dateObj.year
        }
      },
      () => {
        this.loadCalendar(true);
      }
    );
  };

  componentDidMount() {
    this.loadCalendar(true);
  }

  loadCalendar = setState => {
    const { currentDate } = this.state;
    const calendar = buildCalendarMonth(currentDate.month, currentDate.year);
    if (setState) {
      this.setState({ calendar });
    }
    return calendar;
  };

  toggleReminderModal = editReminder => {
    this.setState(state => {
      const newState = {
        showReminderModal: !state.showReminderModal,
        editReminder
      };

      return newState;
    });
  };

  editReminder = reminder => {
    this.toggleReminderModal(reminder);
  };

  insertReminder = reminder => {
    this.setState(state => {
      const { reminders } = state;
      if (reminders[reminder.date]) {
        reminders[reminder.date].push(reminder);
      } else {
        reminders[reminder.date] = [reminder];
      }

      // sorting
      reminders[reminder.date] = reminders[reminder.date].sort(
        (reminder1, reminder2) => {
          const [hour1, minute1] = reminder1.time.split(":");
          const [hour2, minute2] = reminder2.time.split(":");
          if (hour1 === hour2) {
            return minute1 > minute2 ? 1 : -1;
          } else {
            return hour1 > hour2 ? 1 : -1;
          }
        }
      );

      return {
        reminders
      };
    });
  };

  updateReminder = (updatedReminder, previousReminder) => {
    this.setState(
      state => {
        const { reminders } = state;

        const newList = reminders[previousReminder.date].filter(reminder => {
          return reminder.id !== previousReminder.id;
        });

        return {
          reminders: { ...reminders, [previousReminder.date]: newList }
        };
      },
      () => {
        if (updatedReminder) {
          this.insertReminder(updatedReminder);
        }
      }
    );
  };

  clearAllEventsOnDate = date => {
    this.setState(state => {
      let { reminders } = state;
      reminders[date] = [];
      return {
        reminders
      };
    });
  };

  onFormSubmit = async (reminder, previousReminder) => {
    let weather;
    try {
      weather = await loadWeather(reminder.city);
    } catch (err) {
      console.log("Calendar -> updateReminder -> err", err);
    }
    if (reminder.id) {
      this.updateReminder(
        {
          ...previousReminder,
          ...reminder,
          weatherData: weather
        },
        previousReminder
      );
    } else {
      this.insertReminder({
        ...reminder,
        id: `${parseInt(
          Math.random() * 10000
        )}-${reminder.modifiedDate.getTime()}`,
        createdAt: reminder.modifiedDate,
        weatherData: weather
      });
    }
  };

  renderWeekDays = () => {
    return (
      <Fragment>
        <div className="calendar__weekday">SUN</div>
        <div className="calendar__weekday">MON</div>
        <div className="calendar__weekday">TUE</div>
        <div className="calendar__weekday">WED</div>
        <div className="calendar__weekday">THU</div>
        <div className="calendar__weekday">FRI</div>
        <div className="calendar__weekday">SAT</div>
      </Fragment>
    );
  };

  renderDays = () => {
    const { calendar, reminders } = this.state;
    return (
      <Fragment>
        {calendar &&
          calendar.map(date => {
            return (
              <CalendarDay
                date={date}
                clearAllEvents={this.clearAllEventsOnDate}
                key={`calendar-${date.year}-${date.month}-${date.day}`}
                reminders={
                  reminders[`${date.year}-${date.month + 1}-${date.day}`]
                }
                reminderClick={this.editReminder}
              />
            );
          })}
      </Fragment>
    );
  };

  renderControls = () => {
    const { currentDate } = this.state;
    return (
      <div className="calendar__controls">
        <div className="calendar__month-selector">
          <MonthSelector
            month={currentDate.month}
            year={currentDate.year}
            onChange={this.updateMonth}
          />
        </div>

        <div className="calendar__add-reminder">
          <button
            className="add-reminder-btn"
            onClick={() => this.toggleReminderModal()}
          >
            Add Reminder
          </button>
        </div>
      </div>
    );
  };

  render() {
    const { showReminderModal, editReminder } = this.state;
    return (
      <Fragment>
        <div className="calendar">
          {this.renderControls()}

          <div className="calendar__days">
            {this.renderWeekDays()}
            {this.renderDays()}
          </div>
        </div>
        <ReminderForm
          onFormSubmit={this.onFormSubmit}
          showReminderModal={showReminderModal}
          onClose={() => this.toggleReminderModal()}
          editReminder={editReminder}
          removeReminder={reminder => this.updateReminder(null, reminder)}
        />
      </Fragment>
    );
  }
}

export default Calendar;
