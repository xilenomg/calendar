import React from "react";
import CalendarReminder from "./Reminder";

const CalendarDay = props => {
  const { date, reminders, reminderClick, clearAllEvents } = props;

  return (
    <div className="calendar__day">
      <span className="calendar__day__number">{date.day}</span>
      {reminders && reminders.length > 0 && (
        <span
          className="calendar__day__clear-all"
          onClick={() =>
            clearAllEvents(`${date.year}-${date.month + 1}-${date.day}`)
          }
        >
          clear all
        </span>
      )}
      <div className="calendar__reminders">
        {reminders &&
          reminders.map(reminder => (
            <CalendarReminder
              reminder={reminder}
              key={`reminder-${reminder.createdAt}`}
              onClick={reminderClick}
            />
          ))}
      </div>
    </div>
  );
};

CalendarDay.defaultProps = {
  reminders: []
};
CalendarDay.propTypes = {};

export default CalendarDay;
