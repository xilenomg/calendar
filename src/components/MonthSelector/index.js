import React from "react";
import PropTypes from "prop-types";
import { previousMonth, nextMonth } from "../../utils/Date";
import "./style.scss";

const MonthSelector = props => {
  const { month, year, onChange } = props;

  if (typeof month === 'undefined' || !year) {
    return null;
  }

  return (
    <div className="month-selector">
      <span
        className="month-selector__previous"
        onClick={() => {
          onChange(previousMonth(month, year));
        }}
      >
        &lt;
      </span>
      <span className="month-selector__current-month">
        {month + 1} / {year}
      </span>
      <span
        className="month-selector__next"
        onClick={() => {
          onChange(nextMonth(month, year));
        }}
      >
        &gt;
      </span>
    </div>
  );
};

MonthSelector.defaultProps = {
  onChange: () => {}
};

MonthSelector.propTypes = {
  month: PropTypes.number.isRequired,
  year: PropTypes.number.isRequired,
  onChange: PropTypes.func
};

export default MonthSelector;
