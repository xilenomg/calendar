export function getDaysInMonth(month, year) {
  return new Date(year, month + 1, 0).getDate();
}

export function getDaysInMonthBefore(month, year) {
  const previousMonthObj = previousMonth(month, year);
  return getDaysInMonth(previousMonthObj.month, previousMonthObj.year);
}

export function previousMonth(month, year) {
  if (month <= 0) {
    return { month: 11, year: year - 1 };
  } else {
    return { month: month - 1, year: year };
  }
}

export function isValidStringAsDateTime(dateString, timeString) {
  try {
    const date = new Date(`${dateString} ${timeString}`);

    let [year, month, day] = dateString.split("-");
    year = parseInt(year);
    month = parseInt(month);
    day = parseInt(day);

    let [hour, minute] = timeString.split(":");
    hour = parseInt(hour);
    minute = parseInt(minute);

    const newTimeString = `${hour}:${minute}`;
    const newFormattedTime = `${hour < 10 ? `0${hour}` : hour}:${
      minute < 10 ? `0${minute}` : minute
    }`;

    const newDateString = `${year}-${month}-${day}`;

    const compare =
      `${newDateString} ${newTimeString}` ===
      `${date.getFullYear()}-${date.getMonth() +
        1}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}`;

    return compare
      ? {
          date: newDateString,
          time: newTimeString,
          formattedTime: newFormattedTime
        }
      : false;
  } catch (err) {
    console.log("isValidStringAsDateTime -> err", err);
    return false;
  }
}

export function nextMonth(month, year) {
  if (month >= 11) {
    return { month: 0, year: year + 1 };
  } else {
    return { month: month + 1, year: year };
  }
}

export function firstDayWeekDay(month, year) {
  return new Date(year, month, 1).getDay();
}

export function getTodayDate() {
  return new Date();
}

export function getTodayDateAsString() {
  const date = getTodayDate();
  return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
}

export function lastDayWeekDay(month, year) {
  return new Date(year, month + 1, 0).getDay();
}

export function getMonthYearDate(month, year) {
  let date;
  if (!month || !year) {
    date = new Date();
  } else {
    date = new Date(year, month, 1);
  }
  return date;
}

export function buildCalendarMonth(month, year) {
  const monthArray = [];
  const monthBeforeArray = [];
  const monthAfterArray = [];

  const daysInMonth = getDaysInMonth(month, year);

  // adding days into month array
  for (let day = 1; day <= daysInMonth; day++) {
    monthArray.push({ day, month, year });
  }

  const dayInMonthBefore = getDaysInMonthBefore(month, year);
  const currentMonthFirsDayWeekDay = firstDayWeekDay(month, year);
  const currentMonthLastDayWeekDay = lastDayWeekDay(month, year);
  const previousMonthObj = previousMonth(month, year);
  const nextMonthObj = nextMonth(month, year);

  // previous month days displayed on current month calendar
  for (let day = currentMonthFirsDayWeekDay; day > 0; day--) {
    monthBeforeArray.push({ day: dayInMonthBefore - day, ...previousMonthObj });
  }

  // next month days displayed on current month calendar
  let startDay = 1;
  for (let day = currentMonthLastDayWeekDay + 1; day < 7; day++) {
    monthAfterArray.push({ day: startDay++, ...nextMonthObj });
  }

  return [...monthBeforeArray, ...monthArray, ...monthAfterArray];
}
