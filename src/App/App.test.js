import React from 'react';
import { render } from '@testing-library/react';
import App from './index';

test('renders add reminder button', () => {
  const { getByText } = render(<App />);
  const addReminderButton = getByText(/Add reminder/i);
  expect(addReminderButton).toBeInTheDocument();
});

test('renders weekdays names', () => {
  const { getByText } = render(<App />);
  const sunday = getByText(/SUN/i);
  expect(sunday).toBeInTheDocument();
});
