import React from 'react';
import './style.scss';
import Calendar from '../components/Calendar';

function App() {
  return (
    <div className="App">
      <div className="App__calendar">
        <Calendar />
      </div>
    </div>
  );
}

export default App;
